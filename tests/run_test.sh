#! /usr/bin/bash

CC="g++"
cxx="c++17"
flags=""
prof=false

# If no args given just build in release mode
if [ $# == 0 ] ; then
flags+="-O2 -Wall -Wextra -std=$cxx"
else
# We are not doing a release build
flags+="-g -O0 -Wall -Wextra -pedantic -std=$cxx"

for arg in $@
do
case $arg in
    --debug)
    ;;
    --profile)
    flags+=" -pg"
    prof=true
    ;;
    --clean)
    echo "Cleaning binaries"
    for bin in test_*
    do
    rm -fv $bin
    done
    exit 0
    ;;
    *)
    echo "Wrong argument: exiting script"
    exit 1
    ;;
esac
done
fi

set -x

# Compile all the files
for src in *.cpp
do
$CC $flags -o "test_${src%.cpp}" $src
done


# Run all the files
for bin in test_*
do
./$bin
if [ "$prof" == "true" ] ; then
mv "gmon.out" "$bin.out"
gprof "$bin" "$bin.out" > "$bin.profile"
fi
done