# Standard Implementations

I've created this repo to try and recreate some of the standard library implementations.
I plan on trying, in a reasonable manner, to make them fast but I won't fool myself enough to think they will be faster than what is in the standard library.
I am really not planing on implementing **all** of the standard library functionalities.
Only the ones so that I am able to do what I consider as the bare minimum.
I also don't really plan to make my implementation the most portable one.
It will (mostly) work on my machine, but I don't guarentee that it will be the case on yours.  
Note that I've not commented the code much because I don't really plan on distributing the code, neither do I plan on going back to rework it in the future.


## Usage

I make heavy use of some of the gnu programs: `g++` to compile, `gprof` to profile, and not really relevant for you but `gdb` and `valgrind` to debug and check for leaks.
I mention this because I've only implemented a simple makefile in order to compile my files, and a bash script to compile/run the tests.

To compile and run the test just run the script (in the correct folder!):

    cd tests/ && ./run_test.sh

Compiling the `main.cpp` file shouldn't be interesting as it will contais whatever structure I am implementing at the time.  
However if you want to dowload it and try the implementation for yourself you can modify it and compile it for yourself by modifying the provided `Makefile`.

## Done

-   `leo::Vector<T>`
    - Insert: `push_back() emplace_back() operator[]`
    - Constructors: `Vector<>(), Vector<>(const size_t& size, const T& val = T())`
    - Memory alloc: `reserve()`
    - vector iterator: `begin() end()` (enable range based loop)
-   `leo::Array<T, size_t>`
-   `leo::LinkedList<T>`


### Planning
I'm only planning on doing an array implementation and some sort of chained vector structure.
Meaning the project is pretty much done now.
This doesn't exclude the possibility of me reworking some aspects or adding things.